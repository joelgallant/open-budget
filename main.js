'use strict'

const {app, BrowserWindow} = require('electron')
const path = require('path')
const Accounts = require('./data/account')
const Config = require('electron-config')

let conf = new Config({
  defaults: {
    name: 'NoName'
  }
})
let locals = { config: conf.store }

require('electron-pug')({pretty: true}, locals)

let accounts = []
let mainWindow

accounts.push(Accounts.create('Test Accounts', false))

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    height: 600, width: 800, center: false, title: 'Open Budget',
    autoHideMenuBar: true
  })

  mainWindow.loadURL('file://' + path.join(__dirname, 'views/accounts.pug'))
})
