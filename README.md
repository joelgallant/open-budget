# open-budget
[![Build Status](https://travis-ci.org/joelgallant/open-budget.svg?branch=master)](https://travis-ci.org/joelgallant/open-budget)

YNAB4 inspired native budget setting & monitoring app w/ focus on FI

open-budget is meant to replace the depreciated YNAB, as support will end soon.
It does not have the same user-focused goals, but is rather a personal project
that might happen to fit someone else's use case. It's FOSS so you can do what
you would like with it to fit your use case (please contribute back!).

Following the 4 rules of YNAB4, with the same type of workflow. Ultimately the
goal is to enhance that with investment account support, better reports,
automation, goals and eliminating regularly occuring actions.

It will use electron so it's cross-platform, pug templates, stylus css, and
a local PostgreSQL database. I'm using this project as a kind of introduction to
electron for me - happy to accept suggestions.

My work will be based on my own use case (detailed short term budgetting with a
focus on SR%). Has to work on debian & openbox.

### Milestones
Please submit a pull request with additions and I'll let you know if I would
add it.

- Application
    - [ ] Continuous integration & build
    - [ ] Local data persistence
- Accounts
    - [ ] Account creation, editing, closing, deleting
    - [ ] Budget and off-budget accounts
    - [ ] Default account types with special properties
        - [ ] Investment accounts
            - [ ] Investment realtime value and history
        - [ ] Asset class accounts
- Transactions
    - [ ] Transaction creation, editing, closing, deleting
    - [ ] Income categories
    - [ ] Full transaction details
        - [ ] Categories
        - [ ] Transfers
        - [ ] Split transaction
    - [ ] Automated rules for categories, names, memos
    - [ ] Scheduled transactions
    - [ ] Bulk editing
    - [ ] Search
        - [ ] By category
        - [ ] By name
        - [ ] Date filter
- Budgets
    - [ ] Budget creation, editing, closing, deleting
    - [ ] Overflows
    - [ ] Goals
    - [ ] Shared budgets (multiple subbudgets with access to main budget's balance)
    - [ ] Reusing month budgets
- Reports
    - [ ] Net worth reports
    - [ ] Spending reports
    - [ ] Budget reports
    - [ ] Custom reports
    - [ ] Exporting reports
    - [ ] FI calculator
- Files & scripting
    - [ ] YNAB import
    - [ ] File imports
    - [ ] File exports
- Web hookups
    - [ ] File fetching through online banking
    - [ ] Online backup system

### Contributing
Please! Open up an issue or pull request.
