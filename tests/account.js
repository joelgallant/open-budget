'use strict'

const test = require('tape')
const Accounts = require('../data/account')

test('create', function (t) {
  let acc = Accounts.create('name')
  t.equal(acc.name, 'name')
  t.equal(acc.budgetted, true)
  let acc2 = Accounts.create('name2', false)
  t.equal(acc2.name, 'name2')
  t.equal(acc2.budgetted, false)
  t.end()
})

test('add', function (t) {
  let acc = Accounts.create('name', false)
  Accounts.add(acc)
  Accounts.add(acc)
  t.equal(2, Accounts.size())
  Accounts.clear()
  t.end()
})

test('remove', function (t) {
  let acc = Accounts.create('name', false)
  Accounts.add(acc)
  Accounts.remove(acc)
  t.equal(0, Accounts.size())
  t.end()
})
