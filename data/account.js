'use strict'

const rawData = []

function isAccount (account) {
  return (typeof account.name !== 'undefined') &&
         (typeof account.budgetted !== 'undefined')
}

function push (account) {
  if (isAccount(account)) {
    rawData.push(account)
  } else {
    throw new Error('Adding invalid account')
  }
}

function pop (account) {
  let i = rawData.indexOf(account)
  if (i >= 0) {
    rawData.splice(i, 1)
  }
}

module.exports = {
  create: function (name, budgetted) {
    if (typeof budgetted === 'undefined') {
      budgetted = true
    }

    let account = {
      name: name,
      budgetted: budgetted
    }

    return account
  },

  add: function (account) {
    push(account)
  },

  remove: function (account) {
    pop(account)
  },

  clear: function () {
    rawData.length = 0
  },

  size: function () {
    return rawData.length
  }
}
